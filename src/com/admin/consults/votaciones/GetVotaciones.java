package com.admin.consults.votaciones;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.admin.objects.Votaciones;
import com.google.appengine.api.rdbms.AppEngineDriver;

public class GetVotaciones {
	
	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
	private ArrayList<Votaciones> votaciones;
		
	public GetVotaciones(){
		super();	
	}
	
	public ArrayList<Votaciones> getVotaciones (String comunidad,String vigencia) throws  IOException, ClassNotFoundException{
		
		votaciones = new ArrayList<Votaciones>();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM procomvecbbdd.votaciones WHERE comunidad='"+comunidad+"' AND vigencia='"+vigencia+"';" ;

		try {	
		
			rs = (ResultSet) stmt.executeQuery(query);	
			while(rs.next()){			
				votaciones.add(new Votaciones (rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13)));
			}
			
		}catch(SQLException e){
			
			
			
		}
		
		return votaciones;
	}

	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }
		
		
	}
}
