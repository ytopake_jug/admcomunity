package com.admin.consults.resultados;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.google.appengine.api.rdbms.AppEngineDriver;

public class GetResultados {

	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
		
	public GetResultados(){
		super();	
	}
	
	public String getResultado(String id_vot, String opcion) throws  IOException, ClassNotFoundException{
		
		String resultado = "";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT COUNT(*) FROM resultados WHERE id_vot= '"+id_vot+"' AND opcion='"+opcion+"'" ;

		try {	
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);	
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first();			

			resultado = rs.getString(1);
			
		}catch(SQLException e){
			
			
			
		}
		
		return resultado;
	}
	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }	
		
	}

}
