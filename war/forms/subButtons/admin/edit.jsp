<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import = "com.admin.consults.vecinos.GetVecino"%> 
 <%@ page import = "com.admin.objects.Vecinos"%> 
 <%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>editVecinos</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<form id="form" method="post">
	<table>
		<% out.print(getVecino(request.getParameter("dni"))); %>	
		<tr>
			<td style='width: 180px'>
				<table>
					<tr>
						<td style='width: 50%'><button class='marco_button' type='submit' formaction="/delVecino"'>Eliminar</button></td>
						<td style='width: 50%'><button class='marco_button' type='submit' formaction="/addVecino"'>Modificar</button></td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
</form>
</body>
<%!

	public String getVecino (String dni){
	
		String row = "<tr>";
		row = row + "<td class='auto-style12' style='width: 8%'>Nombre</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Apellido 1</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Apellido 2</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>D.N.I</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Usuario</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Contraseña</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Plobación</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>C.P</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Calle</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Número</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Bloque</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Piso</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Puerta</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Comunidad</td>";
		row = row + "</tr>";
		
		
		GetVecino gv = new GetVecino();
		Vecinos vecino=null;
		
		try{
			vecino = gv.getVecino(dni);
		}catch (Exception io){
			
		}finally{		
			
		}
		
		row = row + "<tr>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='nombre' value='"+vecino.getNombre()+"'></td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='apellido1' value='"+vecino.getApellido1()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='apellido2' value='"+vecino.getApellido2()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='dni' value='"+vecino.getDni()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='user' value='"+vecino.getUser()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='password' value='"+vecino.getPassword()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='poblacion' value='"+vecino.getPoblacion()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='cod_postal' value='"+vecino.getCod_postal()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='calle' value='"+vecino.getCalle()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='numero_calle' value='"+vecino.getNumero_calle()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='bloque' value='"+vecino.getBloque()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='piso' value='"+vecino.getPiso()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='puerta' value='"+vecino.getPuerta()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='comunidad' value='"+vecino.getComunidad()+"'</td>";	
		row = row + "</tr>";
		
		gv.closeConnection();
		return row;
	
	}
%>
</html>