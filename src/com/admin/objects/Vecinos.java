package com.admin.objects;

public class Vecinos {
	
	private String id;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String dni;
	private String user;
	private String password;
	private String poblacion;
	private String cod_postal;
	private String calle;
	private String numero_calle;
	private String bloque;
	private String piso;
	private String puerta;
	private String fecha;
	private String comunidad;
	private String telefono;
	private String email;
	
	public Vecinos (String nombre,String apellido1,String apellido2,String dni,String user,String password,String poblacion,String cod_postal,String calle,String numero_calle,String bloque,String piso,String puerta,String Fecha,String comunidad){
		
		super ();
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.dni = dni;
		this.user = user;
		this.password = password;
		this.poblacion = poblacion;
		this.cod_postal = cod_postal;
		this.calle = calle;
		this.numero_calle = numero_calle;
		this.bloque = bloque;
		this.piso = piso;
		this.puerta = puerta;
		this.fecha = fecha;
		this.comunidad = comunidad;
	}
	

	public Vecinos (String nombre,String apellido1,String apellido2,String dni,String user,String password,String poblacion,String cod_postal,String calle,String numero_calle,String bloque,String piso,String puerta,String Fecha,String comunidad,String telefono, String email){
		
		super ();
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.dni = dni;
		this.user = user;
		this.password = password;
		this.poblacion = poblacion;
		this.cod_postal = cod_postal;
		this.calle = calle;
		this.numero_calle = numero_calle;
		this.bloque = bloque;
		this.piso = piso;
		this.puerta = puerta;
		this.fecha = fecha;
		this.comunidad = comunidad;
		this.telefono = telefono;
		this.email = email;
	}
	
	
	public Vecinos (String id, String nombre,String apellido1,String apellido2,String dni,String user,String password,String poblacion,String cod_postal,String calle,String numero_calle,String bloque,String piso,String puerta,String Fecha,String comunidad,String telefono, String email){
		
		super ();
		this.id = id;
		this.nombre = nombre;
		this.apellido1 = apellido1;
		this.apellido2 = apellido2;
		this.dni = dni;
		this.user = user;
		this.password = password;
		this.poblacion = poblacion;
		this.cod_postal = cod_postal;
		this.calle = calle;
		this.numero_calle = numero_calle;
		this.bloque = bloque;
		this.piso = piso;
		this.puerta = puerta;
		this.fecha = fecha;
		this.comunidad = comunidad;
		this.telefono = telefono;
		this.email = email;
	}
	
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}

	public String getCod_postal() {
		return cod_postal;
	}

	public void setCod_postal(String cod_postal) {
		this.cod_postal = cod_postal;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero_calle() {
		return numero_calle;
	}

	public void setNumero_calle(String numero_calle) {
		this.numero_calle = numero_calle;
	}

	public String getBloque() {
		return bloque;
	}

	public void setBloque(String bloque) {
		this.bloque = bloque;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getPuerta() {
		return puerta;
	}

	public void setPuerta(String puerta) {
		this.puerta = puerta;
	}
	
	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getComunidad() {
		return comunidad;
	}

	public void setComunidad(String comunidad) {
		this.comunidad = comunidad;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}	

}
