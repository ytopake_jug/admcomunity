package com.admin.consults.registros;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


import com.google.appengine.api.rdbms.AppEngineDriver;

public class CreateLogs{
	
	Connection con = null;
	Statement stmt = null;

	

	public String createLog(String user, String password, String ip){
		
		String resultado = "";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		String query = "INSERT INTO procomvecbbdd.logs_accesos "
			+ " (user,password,ip)"
			+ " VALUES ('"+user+"','"+password+"','"+ip+"');";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Log a�adido";
	
		} catch (SQLException ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede a�adir el log";
	
		}
		
		return resultado;

	}
	
	public void closeConnection(){

	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }
		
		
	}
}
