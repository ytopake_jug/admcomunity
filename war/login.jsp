<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<body>
	<table style="width: 100%; height: 100%; text-align: center; vertical-align: middle">
  		<tbody>
    		<tr>       
      			<td style="width: 775px; height: 108px;">
       	 			<img src="/images/logo.jpg">
      			</td>
    		</tr>
    		<tr>
      			<td>
        			<form class="login" method = "POST" action = "/login">
    					<input type="text" name = "user" class="login-input" onclick='cleanMensaje();' placeholder="Usuario" autofocus>
    					<input type="password" name = "pass" class="login-input" onclick='cleanMensaje();' placeholder="Contraseña">
    					<input type="hidden" name = "ip" value = <%out.print(request.getRemoteAddr());%>>
    					<input type="hidden" name = "login" value = "yes">		
    					<input type="submit" value="Entrar" class="login-button">
					</form>
					<p></p>
					<p><b>ADMINISTRACIÓN</b></p>
      			</td>
     		</tr>
     		<tr>
       			<td>
         	 		<label id= "mensaje_login" style = "horizontal-align : right; color: #b73131">${error}</label>
       			</td>
      		</tr>
   		</tbody>
	</table>
	<script>
		function cleanMensaje(){
			var mensaje = document.getElementById("mensaje_login");
			mensaje.innerHTML = "";
		}
	</script>
</body>
</html>