<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import = "com.admin.consults.vecinos.GetVecino"%> 
 <%@ page import = "com.admin.objects.Vecinos"%> 
 <%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>editVecinos</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<form id="form" method="post">
	<table>
		<% out.print(getVecino(request.getParameter("id"))); %>	
		<tr>
			<td style='width: 180px'>
				<table>
					<tr>
						<td style='width: 50%'><button class='marco_button' type='submit' formaction="/delVecino"'>Eliminar</button></td>
						<td style='width: 50%'><button class='marco_button' type='submit' formaction="/addVecino"'>Modificar</button></td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
</form>
</body>
<%!

	public String getVecino (String id){
	
		String row = "";
		
		GetVecino gv = new GetVecino();
		Vecinos vecino=null;
		
		try{
			vecino = gv.getVecinoById(id);
		}catch (Exception io){
			
		}finally{		
			
		}
		
		row = row + "<tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Nombre</td><td style='width:10px'><input class='marco_opciones' name='nombre' value='"+vecino.getNombre()+"'></td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Apellido 1</td><td style='width:10px'><input class='marco_opciones' name='apellido1' value='"+vecino.getApellido1()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Apellido 2</td><td style='width:10px'><input class='marco_opciones' name='apellido2' value='"+vecino.getApellido2()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>D.N.I</td><td style='width:10px'><input class='marco_opciones' name='dni' value='"+vecino.getDni()+"'</td>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Usuario</td><td style='width:10px'><input class='marco_opciones' name='user' value='"+vecino.getUser()+"'</td>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Contraseña</td><td style='width:10px'><input class='marco_opciones' name='password' value='"+vecino.getPassword()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Plobación</td><td style='width:10px'><input class='marco_opciones' name='poblacion' value='"+vecino.getPoblacion()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>C.P</td><td style='width:10px'><input class='marco_opciones' name='cod_postal' value='"+vecino.getCod_postal()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Calle</td><td style='width:10px'><input class='marco_opciones' name='calle' value='"+vecino.getCalle()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Número</td><td style='width:10px'><input class='marco_opciones' name='numero_calle' value='"+vecino.getNumero_calle()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Bloque</td><td style='width:10px'><input class='marco_opciones' name='bloque' value='"+vecino.getBloque()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Piso</td><td style='width:10px'><input class='marco_opciones' name='piso' value='"+vecino.getPiso()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Puerta</td><td style='width:10px'><input class='marco_opciones' name='puerta' value='"+vecino.getPuerta()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Comunidad</td><td style='width:10px'><input class='marco_opciones' name='comunidad' value='"+vecino.getComunidad()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Teléfono</td><td style='width:10px'><input class='marco_opciones' name='telefono' value='"+vecino.getTelefono()+"'</td></tr>";
		row = row + "<tr><td  class='texto' style='width: 10%'>Email</td><td style='width:10px'><input class='marco_opciones' name='email' value='"+vecino.getEmail()+"'</td></tr>";	

		row = row + "</tr>";
		
		gv.closeConnection();
		return row;
	
	}
%>
</html>