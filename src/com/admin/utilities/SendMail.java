package com.admin.utilities;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class SendMail extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String user = req.getParameter("user");
		String mensaje = req.getParameter("mensaje");
		String mail = req.getParameter("mail");

		Properties props = new Properties();
	    Session session = Session.getDefaultInstance(props, null);
	    
	    resp.getWriter().print("Sending mail...");

	    try {
	      Message msg = new MimeMessage(session);
	      msg.setFrom(new InternetAddress("procomvec@gmail.com", "Centro de votaciones"));
	      msg.addRecipient(Message.RecipientType.TO,
	                       new InternetAddress(mail, user));
	      msg.setSubject("Your Example.com account has been activated");
	      msg.setText("Tienes una nueva votacion en el siguiente enlace <link>http://votcomunity.appspot.com/match?code="+mensaje+"</link");
	   //   msg.setContent("<h1>Nueva votaci�n</h1>","UTF-8");
	      Transport.send(msg);
	    } catch (AddressException e) {
	    	resp.getWriter().print("AddressException");
	    } catch (MessagingException e) {
	    	resp.getWriter().print("MessagingException");
	    } catch (UnsupportedEncodingException e) {
	    	resp.getWriter().print("UnsupportedEncodingException");
	    }finally {
	    	
	    	 resp.getWriter().print("Mail enviado correctamente");
	    	
	    }
   }
}