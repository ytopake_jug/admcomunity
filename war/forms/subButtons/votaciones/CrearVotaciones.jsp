<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<%@ page import = "com.admin.consults.comunidades.ListaComunidades"%> 
<%@ page import = "com.admin.objects.Comunidades"%> 
<%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Crear Votaciones</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
	<form action='/addVotacion' method="POST" >
		<table style="width: 100%; height: 25%">
			<tr>
				<td class='texto'>Inserte el texto de la votación (máximo 150 caracteres)</td>
			</tr>
			<tr>
				<td>
					<input class='marco_texto' name='pregunta' type="text" style="width: 450px; height: 100px; text-align: center; vertical-align: middle;" >
				</td>
			</tr>
		</table>
		<table style="width: 100%; height: 40%">
			<tr>
				<td class='texto' style="width: 10%">Opción 1</td><td><input class='marco_opciones' name='opcion1'></td>
			</tr>
			<tr>
				<td class='texto' style="width: 10%">Opción 2</td><td><input class='marco_opciones' name='opcion2'></td>
			</tr>
			<tr>
				<td class='texto' style="width: 10%">Opción 3</td><td><input class='marco_opciones' name='opcion3'></td>
			</tr>
			<tr>
				<td class='texto' style="width: 10%">Opción 4</td><td><input class='marco_opciones' name='opcion4'></td>
			</tr>
			<tr>
				<td class='texto' style="width: 10%">Opción 5</td><td><input class='marco_opciones' name='opcion5'></td>
			</tr>
		</table>
		<table style="width: 100%; height: 15%">
			<tr>
				<td class='texto' style="width: 10%">Fecha de Inicio</td><td><input class='marco_fecha' name='fecha_inicio' type='date'></td>
			</tr>
			<tr>
				<td class='texto' style="width: 10%">Fecha de Finalización</td><td><input class='marco_fecha' name='fecha_final' type='date'></td>
			</tr>
			<tr>
				<td class='texto' class='texto' style="width: 10%">Comunidad</td>
				<td>
					<select class='marco_opciones' style='width: 150px' name='comunidad'>
						<%out.print(getComunidades(request.getParameter("user"))); %>
					</select>
				</td>				
			</tr>
			<tr><td><input type="hidden" name="user"><%request.getParameter("user");%></td></tr>
			<tr><td><input class='marco_button' type="submit" value="Crear"></td></tr>
		</table>
	</form>	
</body>
</html>
<%!
	public String getComunidades(String user){
	
		String row = "";	
		ListaComunidades lc = new ListaComunidades();
		ArrayList<String> lista = new ArrayList<String>();
		
		try{
			lista = lc.getNombreComunidades(user);
		}catch(Exception e){
			
		}finally{
			
		}
		
		for (int i = 0; i < lista.size();i++){
			
			row = row +  "<option class='texto' value = "+lista.get(i)+">"+lista.get(i)+"</option>\n";
					
		}		
		
		return row;

}
		
%>	
		
	