<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ page import = "com.admin.consults.comunidades.GetComunidad"%> 
 <%@ page import = "com.admin.objects.Comunidades"%> 
 <%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>editComunidades</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<form id="form" method="post">
	<table>
		<% out.print(getVecino(request.getParameter("comunidad"))); %>	
		<tr>
			<td style='width: 180px'>
				<table>
					<tr>
						<td style='width: 50%'><button class='marco_button' type='submit' formaction="/delComunidad"'>Eliminar</button></td>
						<td style='width: 50%'><button class='marco_button' type='submit' formaction="/editComunidad"'>Modificar</button></td>
					</tr>
				</table>	
			</td>
		</tr>
	</table>
</form>
</body>
<%!
	public String getVecino (String comunidad){
	
		String row = "<tr>";
		row = row + "<td class='auto-style12' style='width: 8%'>Id Comunidad</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Comunidad Autónoma</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Provincia</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Poblacion</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>C.P</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Direccion</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Alta</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Gestoria</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Presidente</td>";
		row = row + "<td class='auto-style12' style='width: 8%'>Teléfono</td>";
	
		row = row + "</tr>";
			
		GetComunidad gc = new GetComunidad();
		Comunidades comunidades = null;
		
		try{
			comunidades = gc.getComunidad(comunidad);
		}catch (Exception io){
			
		}finally{		
			
		}
		
		row = row + "<tr>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='id_comunidad' value='"+comunidades.getId_comunidad()+"'></td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='comunidad_autonoma' value='"+comunidades.getComunidad_autonoma()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='provincia' value='"+comunidades.getProvincia()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='poblacion' value='"+comunidades.getPoblacion()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='cod_postal' value='"+comunidades.getCod_postal()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='direccion' value='"+comunidades.getDireccion()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='fecha_alta' value='"+comunidades.getFecha_alta()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='gestoria' value='"+comunidades.getGestoria()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='presidente' value='"+comunidades.getPresidente()+"'</td>";
		row = row + "<td class='auto-style13' style='width: 8%'><input name='telefono' value='"+comunidades.getTelefono()+"'</td>";

		row = row + "</tr>";
		
		gc.closeConnection();
		return row;
	
	}
%>
</html>