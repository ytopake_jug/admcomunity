package com.admin.consults.vecinos;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.rdbms.AppEngineDriver;

@SuppressWarnings("serial")
public class AddVecino extends HttpServlet {
	
	Connection con = null;
	Statement stmt = null;
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String nombre = req.getParameter("nombre");
		String apellido1 = req.getParameter("apellido1");
		String apellido2 = req.getParameter("apellido2");
		String dni = req.getParameter("dni");
		String user = req.getParameter("user");
		String password = req.getParameter("password");
		String poblacion = req.getParameter("poblacion");
		String cod_postal = req.getParameter("cod_postal");
		String calle = req.getParameter("calle");
		String numero_calle = req.getParameter("numero_calle");
		String bloque = req.getParameter("bloque");
		String piso = req.getParameter("piso");
		String puerta = req.getParameter("puerta");
		String comunidad = req.getParameter("comunidad");
		String telefono = req.getParameter("telefono");
		String email = req.getParameter("email");
		
		PrintWriter out;
		
		try {
			out = resp.getWriter(  );
			out.println("A�adiendo vecino a la base de datos . . . . .");
			out.println(setOnDataBase(nombre, apellido1, apellido2, dni, user, password, poblacion, cod_postal, calle, numero_calle, bloque, piso, puerta, comunidad,telefono,email));
			
		} catch (IOException e) {
			
			out = resp.getWriter(  );
			out.println("No se ha podido a�adir, consulte con el administrador.");

		}finally {
						
		}
	}
	

	public String setOnDataBase(String nombre, String apellido1, String apellido2, String dni, String user, String password, String poblacion, String cod_postal, String calle, String numero_calle, String bloque, String piso, String puerta, String comunidad, String telefono, String email){
		
		String resultado = "";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		String query = "INSERT INTO procomvecbbdd.vecinos "
			+ " (nombre,apellido1,apellido2,dni,user,password,poblacion,cod_postal,calle,numero_calle,bloque,piso,puerta,comunidad,telefono,email)"
			+ " VALUES ('"+nombre+"','"+apellido1+"','"+apellido2+"','"+dni+"','"+user+"','"+password+"','"+poblacion+"','"+cod_postal+"','"+calle+"','"+numero_calle+"','"+bloque+"','"+piso+"','"+puerta+"','"+comunidad+"','"+telefono+"','"+email+"');";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Vecino a�adido";
	
		} catch (Exception ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede a�adir el vecino";
	
		}
		
		return resultado;

	}
	
}
