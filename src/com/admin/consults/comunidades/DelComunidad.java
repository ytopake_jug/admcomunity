package com.admin.consults.comunidades;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.rdbms.AppEngineDriver;

@SuppressWarnings("serial")
public class DelComunidad extends HttpServlet {
	
	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
		
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String id_comunidad = req.getParameter("id_comunidad");
		String comunidad_autonoma = req.getParameter("comunidad_autonoma");
		String provincia = req.getParameter("provincia");
		String poblacion = req.getParameter("poblacion");
		String cod_postal = req.getParameter("cod_postal");
		String direccion = req.getParameter("direccion");
		String gestoria = req.getParameter("gestoria");
		String presidente = req.getParameter("presidente");
		String telefono = req.getParameter("telefono");
		
		PrintWriter out;
		
		try {
			out = resp.getWriter(  );
			out.println("Eliminando comunidad a la base de datos . . . . . " + id_comunidad + "/" + comunidad_autonoma + "/" + provincia + "/" +poblacion + "/" + cod_postal + "/" + direccion );
			out.println(delOfDataBase(id_comunidad, comunidad_autonoma,provincia, poblacion, cod_postal, direccion, gestoria, presidente, telefono));
			
		} catch (IOException e) {
			
			out = resp.getWriter(  );
			out.println("No se ha podido eliminar, consulte con el administrador.");

		}finally {
						
		}
	
	}
	
	public String delOfDataBase(String id_comunidad, String comunidad_autonoma,String provincia, String poblacion, String cod_postal, String direccion, String gestoria, String presidente, String telefono){
		
		String resultado = "";
		String id="";
		
		try {
			
			id = getIdComunity(id_comunidad, comunidad_autonoma, provincia, poblacion, cod_postal, direccion, gestoria, presidente, telefono);
			
		} catch (Exception e) {
		}
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}
			
		String query = "DELETE FROM procomvecbbdd.comunidades WHERE id = '"+id+"'";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Comunidad "+id+" eliminada";
	
		} catch (Exception ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede eliminar la comunidad";
	
		}
		
		return resultado;

	}

	public String getIdComunity(String id_comunidad, String comunidad_autonoma, String provincia, String poblacion, String cod_postal, String direccion, String gestoria, String presidente, String telefono) throws SQLException{
		
		String resultado ="";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		String query = "SELECT id FROM procomvecbbdd.comunidades WHERE id_comunidad = '"+id_comunidad+"' AND comunidad_autonoma='"+comunidad_autonoma+"';";// AND poblacion='"+poblacion+"' AND cod_postal = '"+cod_postal+"' AND direccion = '"+direccion+"'  AND fecha_alta = '"+fecha_alta+"' AND gestoria='"+gestoria+"' AND presidente = '"+presidente+"' AND telefono = '"+telefono+"';";

		
		try {
			
			stmt = con.createStatement();
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first(); 	
		    resultado = rs.getString(1);
		    
		    
		}catch(SQLException e){
			
			return resultado =  "no existe";
		
		}finally {		
			
			stmt.close();
			con.close();
		}
	
	
		return resultado;
	}
	
}
