package com.admin.utilities;

import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMailObject {
	
	public SendMailObject() {
	
		super();
			
	}
	
	public String send(String id, String user, String to, String mensaje, String id_votacion) {
		
		String result ="";
		
		Properties props = new Properties();
	    Session session = Session.getDefaultInstance(props, null);

	    try {
	      Message msg = new MimeMessage(session);
	      msg.setFrom(new InternetAddress("procomvec@gmail.com", "Centro de votaciones"));
	      msg.addRecipient(Message.RecipientType.TO,
	                       new InternetAddress(to, user));
	      msg.setSubject("Your Example.com account has been activated");
	      msg.setText("Tienes una nueva votacion en el siguiente enlace: \n\nhttp://votcomunity.appspot.com/match?code="+mensaje+"&id_votacion="+id_votacion+"&id_vecino="+id+"\nPor favor, no responda a este mensaje.");

	      Transport.send(msg);
	
	    } catch (AddressException e) {
	    	
	    	result = "AddressException";
	    	
	    } catch (MessagingException e) {
	    	
	    	result = "MessagingException";
	    	
	    } catch (UnsupportedEncodingException e) {
	    	
	    	result = "UnsupportedEncodingException";
	    	
	    }finally {
	    	
	    	result = "mail enviado correctamente";
	    	
	    }
		
		return result;
		
	}
}
