package com.admin.consults.vecinos;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.rdbms.AppEngineDriver;

@SuppressWarnings("serial")
public class DelVecino extends HttpServlet {
	
	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String nombre = req.getParameter("nombre");
		String apellido1 = req.getParameter("apellido1");
		String apellido2 = req.getParameter("apellido2");
		String dni = req.getParameter("dni");
		String user = req.getParameter("user");
		String password = req.getParameter("password");
		String poblacion = req.getParameter("poblacion");
		String cod_postal = req.getParameter("cod_postal");
		String calle = req.getParameter("calle");
		String numero_calle = req.getParameter("numero_calle");
		String bloque = req.getParameter("bloque");
		String piso = req.getParameter("piso");
		String puerta = req.getParameter("puerta");
		String comunidad = req.getParameter("comunidad");
		
		PrintWriter out;
		
		try {
			out = resp.getWriter(  );
			out.println("Eliminando vecino a la base de datos . . . . . " + nombre + "/" + apellido1 + "/" + apellido2 + "/" + dni + "/" + comunidad );
			out.println(delOfDataBase(nombre, apellido1, apellido2, dni, user, password, poblacion, cod_postal, calle, numero_calle, bloque, piso, puerta, comunidad));
			
		} catch (IOException e) {
			
			out = resp.getWriter(  );
			out.println("No se ha podido eliminar, consulte con el administrador.");

		}finally {
						
		}
	}
	

	public String delOfDataBase(String nombre, String apellido1, String apellido2, String dni, String user, String password, String poblacion, String cod_postal, String calle, String numero_calle, String bloque, String piso, String puerta, String comunidad){
		
		String resultado = "";
		String id="";
		
		try {
			
			id = getIdVecino(nombre, apellido1, apellido2, dni, user, password, poblacion, cod_postal, calle, numero_calle, bloque, piso, puerta, comunidad);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//String query = "DELETE FROM procomvecbbdd.vecinos WHERE nombre = '"+nombre+"' AND apellido1='"+apellido1+"' AND apellido2='"+apellido2+"' AND dni = '"+dni+"' AND user = '"+user+"' AND password = '"+password+"';";
		
		String query = "DELETE FROM procomvecbbdd.vecinos WHERE id = '"+id+"'";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Vecino eliminado";
	
		} catch (Exception ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede eliminar el vecino";
	
		}
		
		return resultado;
		//return id;

	}
	

	public String getIdVecino(String nombre, String apellido1, String apellido2, String dni, String user, String password, String poblacion, String cod_postal, String calle, String numero_calle, String bloque, String piso, String puerta, String comunidad) throws SQLException{
		
		String resultado ="";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	//	String query = "SELECT id FROM procomvecbbdd.vecinos WHERE nombre = '"+nombre+"' AND apellido1='"+apellido1+"' AND apellido2='"+apellido2+"' AND dni = '"+dni+"' AND user = '"+user+"' AND password = '"+password+"';";
		String query = "SELECT id FROM procomvecbbdd.vecinos WHERE nombre = '"+nombre+"' AND apellido1='"+apellido1+"' AND apellido2='"+apellido2+"' AND dni = '"+dni+"' AND comunidad = '"+comunidad+"';";// AND password = '"+password+"';";

		
		try {
			
			stmt = con.createStatement();
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first(); 	
		    resultado = rs.getString(1);
		    
		    
		}catch(SQLException e){
			
			return resultado =  "no existe";
		
		}finally {		
			
			stmt.close();
			con.close();
		}
	
	
		return resultado;
	}
	
}
