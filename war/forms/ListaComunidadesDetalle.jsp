<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "com.admin.consults.comunidades.GetComunidad"%> 
<%@ page import = "com.admin.objects.Comunidades"%> 
<%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ListaComunidades</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<table>
	<% out.print(getListaComunidades(request.getParameter("comunidad"))); %>
</table>
</body>



<%!

	public String getListaComunidades (String comunidad){
	
		String row = "<tr><td class='auto-style12'>Id Comunidad</td><td class='auto-style12'>Comunidad Aut�noma</td><td class='auto-style12'>Provincia</td><td  class='auto-style12'>Poblacion</td><td  class='auto-style12'>C.P</td>";
		row = row + "<td  class='auto-style12'>Direcci�n</td><td  class='auto-style12'>Alta</td><td  class='auto-style12'>Gestoria</td><td  class='auto-style12'>Presidente</td>";
		row = row + "<td  class='auto-style12'>Telefono</td>";
		
		GetComunidad gc = new GetComunidad();
		
		try{
		
			Comunidades com = gc.getComunidad(comunidad);	
			row = row + "<tr> " + "\n";
//			row = row + "<td class='auto-style15' style='width: 10%'></td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%';pointer: cursor' onclick='goListaComunidadDetalle(\""+com.getId_comunidad()+"\")'>"+com.getId_comunidad()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getComunidad_autonoma()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getProvincia()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getPoblacion()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getCod_postal()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getDireccion()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getFecha_alta()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getGestoria()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getPresidente()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+com.getTelefono()+"</td>" + "\n";
			row = row + "<tr></tr><tr><td  class='auto-style11' style='width: 3%' onclick='editar(\""+com.getId_comunidad()+"\")'>Editar</td><td class='auto-style11' id='users' onclick='users(\""+com.getId_comunidad()+"\")'>Usuarios</td><td class='auto-style11' id='delete' onclick='delete(\""+com.getId_comunidad()+"\")'>Eliminar</td></tr>" + "\n";
			row = row + "</tr>";
	
		}catch(Exception e){
			
		}			
			
		return row;
	}
%>
<script>

	function editar (comunidad) {
		
		location.href = "/forms/subButtons/comunidades/edit.jsp?comunidad="+comunidad;	
		
	}
	
	function users (comunidad) {
		
		location.href = "/forms/ListaVecinosByComunity.jsp?comunidad="+comunidad;	
		
	}

</script>
</html>