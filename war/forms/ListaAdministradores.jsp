<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ page import = "com.admin.consults.vecinos.ListaVecinos"%> 
 <%@ page import = "com.admin.objects.Vecinos"%> 
 <%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ListaVecinos</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<table>
	<tr>
		<table><% out.print(getListaAdministradores()); %></table>
	</tr>
</table>
</body>
<%!

	public String getListaAdministradores (){
	
		String row = "<tr><td></td><td class='auto-style12'>Nombre</td><td class='auto-style12'>Apellido 1</td><td  class='auto-style12'>Apellido 2</td><td  class='auto-style12'>D.N.I</td>";
		row = row + "<td  class='auto-style12'>Usuario</td><td  class='auto-style12'>Contraseña</td><td  class='auto-style12'>Plobación</td><td  class='auto-style12'>C.P</td>";
		row = row + "<td  class='auto-style12'>Calle</td><td  class='auto-style12'>Numero</td><td  class='auto-style12'>Bloque</td><td  class='auto-style12'>Piso</td>";
		row = row + "<td  class='auto-style12'>Puerta</td><td class='auto-style12'>Alta</td><td class='auto-style12'>Comunidad</td>";
		
		
		ListaVecinos lv = new ListaVecinos();
		ArrayList<Vecinos> vecinos = new ArrayList<>();
		
		try{
			//vecinos = lv.getListaVecinos("comun1");
			vecinos = lv.getListaVecinos();
		}catch (Exception io){
			
		}finally{		
			
		}
		
		for (int i = 0; i < vecinos.size(); i++){
			
			row = row + "<tr> " + "\n";
			row = row + "<td  class='auto-style11' style='width: 3%' onclick='editar(\""+vecinos.get(i).getDni()+"\")'>editar</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getNombre()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getApellido1()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getApellido2()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getDni()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getUser()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getPassword()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getPoblacion()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getCod_postal()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getCalle()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getNumero_calle()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getBloque()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getPiso()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getPuerta()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getFecha()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getComunidad()+"</td>" + "\n";
			row = row + "<td class='auto-style15' style='width: 10%'></td>" + "\n";
			row = row + "</tr>";
		}
		
		lv.closeConnection();
		return row;
	
	}
%>
<script>

	function editar (dni) {
		
		location.href = "/forms/subButtons/vecinos/edit.jsp?dni="+dni;	
		//alert (dni);
		
	}

</script>
</html>