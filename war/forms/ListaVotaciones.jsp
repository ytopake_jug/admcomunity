<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.admin.consults.comunidades.GetComunidad"%> 
<%@ page import = "com.admin.consults.votaciones.GetVotaciones"%> 
<%@ page import = "com.admin.objects.Vecinos"%> 
<%@ page import = "com.admin.objects.Votaciones"%>
<%@ page import = "java.util.ArrayList"%> 
<html>
<head>
  <title>Google Charts</title>
  <link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<table>
	<tr>
		<td>
			<table><% out.print(getComunidades(request.getParameter("user"),request.getParameter("vigencia")));%></table>
		</td>
	</tr>
</table>
</body>
<%!
	
	public String getComunidades(String user,String vigencia){
		
		String row = "";

		ArrayList<String> comunidades = new ArrayList<String>();
		
		GetComunidad gc = new GetComunidad();
		
		try{
			
			comunidades = gc.getComunidades(user);
			
		}catch (Exception e){
			
			
			
		}
		
		
		for (int i = 0; i<comunidades.size(); i++){
				
			String votacion = getVotacion(comunidades.get(i),vigencia);		
			row = row + votacion;
				
		}
		
		return row;
		
	}

	public String getVotacion (String comunidad,String vigencia){
		
		String row = "";
		
		ArrayList<Votaciones> votaciones = new ArrayList<Votaciones>();
		
		GetVotaciones gv = new GetVotaciones();
		
		try{
			
			votaciones = gv.getVotaciones(comunidad,vigencia); 
			
		}catch (Exception e){
			
		}
		
		if (votaciones.size()>0){
			
			row = "<tr><td></td><td class='auto-style12'>Pregunta</td><td class='auto-style12'>Fecha de publicación</td><td  class='auto-style12'>Fecha de finalización</td><td  class='auto-style12'>Comunidad</td>";
		
		}
		
		for (int i = 0; i < votaciones.size(); i++){
			
			row = row + "<tr>";
			row = row + "<td  class='auto-style11' style='width: 3%' onclick='graficos(\""+votaciones.get(i).getId_vot()+"\",\""+votaciones.get(i).getPregunta()+"\",\""+votaciones.get(i).getComunidad()+"\")'><img src='/images/result.png'></td>" + "\n";
			row = row + "<td class='auto-style13'>"+votaciones.get(i).getPregunta()+"</td><td class='auto-style13'>"+votaciones.get(i).getFecha_creacion()+"</td><td class='auto-style13'>"+votaciones.get(i).getFecha_final()+"</td><td class='auto-style13'>"+votaciones.get(i).getComunidad()+"</td>";
			
			if (vigencia.equals("si")){
				
				row = row + "<td style='width: 3% ; cursor: pointer;' onclick='del(\""+votaciones.get(i).getId_vot()+"\")'><img src='/images/delete.png'></td><td style='width: 3% ; cursor: pointer;' onclick='stop(\""+votaciones.get(i).getId_vot()+"\")'><img src='/images/stop.png'></td>" + "\n";
				
			}else{
				
				if (vigencia.equals("no")){
					
					row = row + "<td style='width: 3% ; cursor: pointer;' onclick='del(\""+votaciones.get(i).getId_vot()+"\")'><img src='/images/delete.png'></td>" + "\n";

					
				}else{
					
					if (vigencia.equals("st")){
						
						row = row + "<td style='width: 3% ; cursor: pointer;' onclick='del(\""+votaciones.get(i).getId_vot()+"\")'><img src='/images/delete.png'></td><td style='width: 3% ; cursor: pointer;' onclick='send(\""+votaciones.get(i).getComunidad()+"\",\""+votaciones.get(i).getId_vot()+"\")'><img src='/images/startup.png'></td>" + "\n";
					
					}
					
				}
				
			}
			
			row = row + "</tr>";
			
		}
		
		return row;
	}

%>

<script>
	
	function graficos (id,pregunta,comunidad){
	
		location.href = "/forms/subButtons/resultados/resultados.jsp?id_vot="+id+"&pregunta="+pregunta+"&comunidad="+comunidad+"&nopcion1="+comunidad+"&nopcion2="+comunidad+"&nopcion3="+comunidad+"&nopcion4="+comunidad+"&nopcion5="+comunidad;
	
	}
	
	function del (id){
		
		location.href = "/delVotacion?id_votacion="+id;
	
	}
	
	function stop (id){
		
		location.href = "/stopVotacion?id_votacion="+id;
	
	}
	
	function send (id,id_vot){
		
	//	location.href = "/forms/ListaVecinosByComunity.jsp?comunidad="+id+"&id_vot="+id_vot;
		location.href = "/forms/ListaVecinosEnviar.jsp?comunidad="+id+"&id_vot="+id_vot;;
	
	}

</script>


</html>