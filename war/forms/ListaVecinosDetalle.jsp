<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 <%@ page import = "com.admin.consults.vecinos.GetVecino"%> 
 <%@ page import = "com.admin.objects.Vecinos"%> 
 <%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ListaVecinos</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<table>
	<tr>
		<table><% out.print(getListaVecinos(request.getParameter("id"))); %></table>
	</tr>
</table>
</body>
<%!

	public String getListaVecinos (String id){
	
		String row = "<tr><td class='auto-style12'>Nombre</td><td class='auto-style12'>Apellido 1</td><td  class='auto-style12'>Apellido 2</td><td  class='auto-style12'>D.N.I</td>";
		row = row + "<td  class='auto-style12'>Usuario</td><td  class='auto-style12'>Contraseña</td><td  class='auto-style12'>Plobación</td><td  class='auto-style12'>C.P</td>";
		row = row + "<td  class='auto-style12'>Calle</td><td  class='auto-style12'>Numero</td><td  class='auto-style12'>Bloque</td><td  class='auto-style12'>Piso</td>";
		row = row + "<td  class='auto-style12'>Puerta</td><td class='auto-style12'>Alta</td><td class='auto-style12'>Comunidad</td>";
		
		GetVecino vecinos = new GetVecino();
		
		try{
			Vecinos vec = vecinos.getVecinoById(id);
			row = row + "<tr> " + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getNombre()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getApellido1()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getApellido2()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getDni()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getUser()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getPassword()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getPoblacion()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getCod_postal()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getCalle()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getNumero_calle()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getBloque()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getPiso()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getPuerta()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getFecha()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vec.getComunidad()+"</td>" + "\n";
			row = row + "<tr></tr><tr><td  class='auto-style11' style='width: 3%' onclick='editar(\""+vec.getDni()+"\")'>Editar</td><td class='auto-style11' id='delete' onclick='delete(\""+vec.getId()+"\")'>Eliminar</td></tr>" + "\n";
			row = row + "</tr>";
		
		}catch (Exception io){
			
		}finally{		
			
		}	
		
		return row;
	}
%>
<script>

	function editar (dni) {
		
		location.href = "/forms/subButtons/vecinos/edit.jsp?dni="+dni;	
		//alert (dni);	
	}

</script>
</html>