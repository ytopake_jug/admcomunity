package com.admin.consults;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.google.appengine.api.rdbms.AppEngineDriver;

public class ConsultUser {

	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
		
	public ConsultUser(){
		super();	
	}
	
	public String consultarUsuario(String user, String pass) throws  IOException, ClassNotFoundException{
		
		String resultado ="ok";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT tipo FROM procomvecbbdd.adminusers WHERE user = '"+user+"' AND password = '"+pass+"';" ;

		try {		
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);	
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first(); 	
		    resultado = rs.getString(1);					
		}catch(SQLException e){
			return resultado =  "Alguno de los datos son incorrectos o el usuario no existe";
		}finally {			
			//closeConnection();
		}
	
	
		return resultado;
	}
	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }
		
		
	}

}
