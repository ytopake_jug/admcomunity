<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.admin.consults.resultados.GetResultados"%> 

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table>
		<tr>
			<td width="5%">
				<div id='id_vot' style="display: none;"><%out.print(request.getParameter("id_vot"));%></div>
			</td>
			<td width="25%">
				<div id='pregunta' style="display: none;"><%out.print(request.getParameter("pregunta"));%></div>
			</td>
			<td width="10%">
				<div id='comunidad' style="display: none;"><%out.print(request.getParameter("comunidad"));%></div>
			</td>
			<td width="10%">
				<div id='opcion1' style="display: none;"><%out.print(getResultados(request.getParameter("id_vot"),"opcion1"));%></div>
			</td>
			<td width="10%">
				<div id='opcion2' style="display: none;"><%out.print(getResultados(request.getParameter("id_vot"),"opcion2"));%></div>
			</td>
			<td width="10%">
				<div id='opcion3' style="display: none;"><%out.print(getResultados(request.getParameter("id_vot"),"opcion3"));%></div>
			</td>
			<td width="10%">
				<div id='opcion4' style="display: none;"><%out.print(getResultados(request.getParameter("id_vot"),"opcion4"));%></div>
			</td>
			<td width="10%">
				<div id='opcion5' style="display: none;"><%out.print(getResultados(request.getParameter("id_vot"),"opcion5"));%></div>
			</td>
		</tr>
		<tr>
	<!--  	<td>
				<object style='width: 700px ; height: 580px' type='text/html' data='resultados_rosco.jsp'></object>
			</td>
			<td>
				<object style='width: 800px; height: 580px' type='text/html' data='resultados_barras.jsp'></object>			
			</td>	-->
			
			<td style="width: 50%; height: 650px;">
				<div style="width: 100%; height: 650px;" id="piechart"></div>
			</td>
			<td style="width: 50%; height: 650px">
				<div style="width: 100%; height: 650px" id="GraficoGoogleChart-ejemplo-1"></div>
			</td>
		</tr>
	</table>
</body>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
    	  
    	var id_vot = document.getElementById("id_vot").innerHTML;
    	var pregunta = document.getElementById("pregunta").innerHTML;
    	var comunidad = document.getElementById("comunidad").innerHTML;
    	var opcion1 = document.getElementById("opcion1").innerHTML;
    	var opcion2 = document.getElementById("opcion2").innerHTML;
    	var opcion3 = document.getElementById("opcion3").innerHTML;
    	var opcion4 = document.getElementById("opcion4").innerHTML;
    	var opcion5 = document.getElementById("opcion5").innerHTML;

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per day'],
          ['SÍ', parseInt(opcion1)],
          ['NO', parseInt(opcion2)],
          ['ABS', parseInt(opcion3)]
        ]);

        var options = {
          title: pregunta,
          is3D: true
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
</script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
<script>
   google.load("visualization", "1", {packages:["corechart"]});
   google.setOnLoadCallback(dibujarGrafico);
   function dibujarGrafico() {
	   
	 var id_vot = document.getElementById("id_vot").innerHTML;
     var pregunta = document.getElementById("pregunta").innerHTML;
     var comunidad = document.getElementById("comunidad").innerHTML; 
     var opcion1 = document.getElementById("opcion1").innerHTML;
 	 var opcion2 = document.getElementById("opcion2").innerHTML;
 	 var opcion3 = document.getElementById("opcion3").innerHTML;
 	 var opcion4 = document.getElementById("opcion4").innerHTML;
 	 var opcion5 = document.getElementById("opcion5").innerHTML;
     // Tabla de datos: valores y etiquetas de la gráfica
     var data = google.visualization.arrayToDataTable([
       ['Texto', 'Número de votos'],
       ['SÍ', parseInt(opcion1)],
       ['NO', parseInt(opcion2)],
       ['ABS', parseInt(opcion3)]   
     ]);
     var options = {
       title: pregunta
     }
     // Dibujar el gráfico
     new google.visualization.ColumnChart( 
     //ColumnChart sería el tipo de gráfico a dibujar
       document.getElementById('GraficoGoogleChart-ejemplo-1')
     ).draw(data, options);
   }
 </script> 
 <%!
 
 	public String getResultados(String id_vot, String opcion){
	 
	 String row = "";
	 
	 GetResultados gr = new GetResultados();
	 
	 try{	
	 	row = gr.getResultado(id_vot, opcion);
	 }catch(Exception e){
		 
	 }
	 
	 gr.closeConnection();
	 
	 return row;
	 
 } 
 
 %>
</html>