package com.admin.objects;

public class Comunidades {

	private String id_comunidad;
	private String comunidad_autonoma;
	private String provincia;
	private String poblacion;
	private String cod_postal;
	private String direccion;
	private String fecha_alta;
	private String gestoria;
	private String presidente;
	private String telefono;
	
	
	public Comunidades (String id_comunidad, String comunidad_autonoma, String provincia, String poblacion, String cod_postal, String direccion, String fecha_alta, String gestoria, String presidente, String telefono) {
		
		super();
		
		this.id_comunidad = id_comunidad;
		this.comunidad_autonoma = comunidad_autonoma;
		this.provincia = provincia;
		this.poblacion = poblacion;
		this.cod_postal = cod_postal;
		this.direccion = direccion;
		this.fecha_alta = fecha_alta;
		this.gestoria = gestoria;
		this.presidente = presidente;
		this.telefono = telefono;
		
	}



	public String getId_comunidad() {
		return id_comunidad;
	}


	public void setId_comunidad(String id_comunidad) {
		this.id_comunidad = id_comunidad;
	}


	public String getComunidad_autonoma() {
		return comunidad_autonoma;
	}


	public void setComunidad_autonoma(String comunidad_autonoma) {
		this.comunidad_autonoma = comunidad_autonoma;
	}


	public String getProvincia() {
		return provincia;
	}

	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}


	public String getPoblacion() {
		return poblacion;
	}


	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}


	public String getCod_postal() {
		return cod_postal;
	}


	public void setCod_postal(String cod_postal) {
		this.cod_postal = cod_postal;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getFecha_alta() {
		return fecha_alta;
	}


	public void setFecha_alta(String fecha_alta) {
		this.fecha_alta = fecha_alta;
	}


	public String getGestoria() {
		return gestoria;
	}


	public void setGestoria(String gestoria) {
		this.gestoria = gestoria;
	}


	public String getPresidente() {
		return presidente;
	}


	public void setPresidente(String presidente) {
		this.presidente = presidente;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	
	
}
