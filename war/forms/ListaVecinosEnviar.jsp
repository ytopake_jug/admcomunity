<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
 <%@ page import = "com.admin.consults.vecinos.ListaVecinos"%> 
 <%@ page import = "com.admin.objects.Vecinos"%> 
 <%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ListaVecinos</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<table>
	<tr>
		<td  class='auto-style11' style='width: 3%' onclick='enviar()'>Enviar</td>
	</tr>
	<tr>
	</tr>
	<% out.print(getListaVecinos(request.getParameter("comunidad"))); %>
	<tr>
		<td>
			<div id='comunidad' style="display: none;" ><% out.print(request.getParameter("comunidad"));%></div>
			<div id='id_vot' style="display: none;" ><% out.print(request.getParameter("id_vot"));%></div>
		</td>
	</tr>
</table>
</body>
<%!

	public String getListaVecinos (String comunidad){
	
		String row = "<tr><td class='auto-style12'>Nombre</td><td class='auto-style12'>Apellido 1</td><td  class='auto-style12'>Apellido 2</td><td  class='auto-style12'>Telefono</td><td class='auto-style12'>Email</td>";
		
		ListaVecinos lv = new ListaVecinos();
		ArrayList<Vecinos> vecinos = new ArrayList<>();
		
		try{
			vecinos = lv.getListaVecinos(comunidad);
		}catch (Exception io){
			
		}finally{		
			
		}
		
		for (int i = 0; i < vecinos.size(); i++){
			
			row = row + "<tr> " + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getNombre()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getApellido1()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getApellido2()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getTelefono()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+vecinos.get(i).getEmail()+"</td>" + "\n";
			row = row + "</tr>";
		}
		
		lv.closeConnection();
		return row;
	
	}
%>
<script>

	function enviar () {
		
		var comunidad = document.getElementById("comunidad").innerHTML;
		var id_vot = document.getElementById("id_vot").innerHTML;
		
		location.href = "/forms/subButtons/votaciones/SendVotacion.jsp?comunidad="+comunidad+"&id_vot="+id_vot;
		
	}

</script>
</html>