<html>
<head>
  <title>Google Charts</title>
  <link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<table>
	<tr>
		
	</tr>
</table>
<script type="text/javascript" src="https://www.google.com/jsapi"></script> 
<script>
   google.load("visualization", "1", {packages:["corechart"]});
   google.setOnLoadCallback(dibujarGrafico);
   function dibujarGrafico() {
     // Tabla de datos: valores y etiquetas de la gr�fica
     var data = google.visualization.arrayToDataTable([
       ['Texto', 'Valor num�rico'],
       ['Texto1', 20.21],
       ['Texto2', 4.28],
       ['Texto3', 17.26],
       ['Texto4', 10.25]    
     ]);
     var options = {
       title: 'Resultado Votaciones'
     }
     // Dibujar el gr�fico
     new google.visualization.ColumnChart( 
     //ColumnChart ser�a el tipo de gr�fico a dibujar
       document.getElementById('GraficoGoogleChart-ejemplo-1')
     ).draw(data, options);
   }
 </script> 
<body>
<table>
	<tr>
		<td><div id="GraficoGoogleChart-ejemplo-1" style="width: 800px; height: 600px"></div></td>
	</tr>
</table>
</body>
</html>