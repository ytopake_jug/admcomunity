package com.admin.comunity;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import com.admin.consults.ConsultUser;
import com.admin.consults.registros.CreateLogs;

@SuppressWarnings("serial")
public class Login extends HttpServlet {
	
	private String tipo;
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException,ServletException {
		
		String user = req.getParameter("user");
		String pass = req.getParameter("pass");
		String ip = req.getParameter("ip");
		PrintWriter out = resp.getWriter(  );
		out.println(user);
		out.println(pass);
		
		ConsultUser consult =  new ConsultUser();
		try{
			tipo = consult.consultarUsuario(user,pass);
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}finally {
			consult.closeConnection();				
		}
		
		CreateLogs cl = new CreateLogs();
		try{
			cl.createLog(user, pass, ip);
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			cl.closeConnection();				
		}
		
		if(tipo.equals("Alguno de los datos son incorrectos o el usuario no existe")) {			
			PrintWriter out2 = resp.getWriter(); 
			RequestDispatcher rd=req.getRequestDispatcher("login.jsp");
		    req.setAttribute("error", tipo); 		        		    
		    try {
		    	rd.forward(req,resp);
			} catch (ServletException e) {		
				e.printStackTrace();
			}finally {
				out2.close();
			}
		}else {
			if(tipo.equals("editor")) { 
				createTable(req, resp, user, tipo);
			}else {
				//Debe crear otra p�gina diferente para admin
				createTable(req, resp, user, tipo);		
			}
		}
	}
	
	public void createTable(HttpServletRequest req , HttpServletResponse resp, String user, String tipo) throws IOException, ServletException{
		ServletContext context= getServletContext();
		RequestDispatcher rd= context.getRequestDispatcher("/adminpage.jsp?user="+user+"&tipo="+tipo);
		context.setAttribute("user", user);
		context.setAttribute("tipo", tipo);
		rd.forward(req, resp);
	}
	
	public void createTable_no_tools(HttpServletRequest req , HttpServletResponse resp,String user, String tipo) throws IOException, ServletException{
		ServletContext context= getServletContext();
		RequestDispatcher rd= context.getRequestDispatcher("/adminpage.jsp?user="+user+"&tipo="+tipo);
		context.setAttribute("user", user);
		context.setAttribute("tipo", tipo);
		rd.forward(req, resp);
	}
}
