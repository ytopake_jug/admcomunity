package com.admin.consults.comunidades;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.rdbms.AppEngineDriver;

@SuppressWarnings("serial")
public class AddComunidad extends HttpServlet {
	
	Connection con = null;
	Statement stmt = null;
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String id_comunidad = req.getParameter("id_comunidad");
		String comunidad_autonoma = req.getParameter("comunidad_autonoma");
		String provincia = req.getParameter("provincia");
		String poblacion = req.getParameter("poblacion");
		String cod_postal = req.getParameter("cod_postal");
		String direccion = req.getParameter("direccion");
//		String fecha_alta = req.getParameter("fecha_alta");
		String gestoria = req.getParameter("gestoria");
		String presidente = req.getParameter("presidente");
		String telefono = req.getParameter("telefono");
		
		PrintWriter out;
		
		try {
			out = resp.getWriter(  );
			out.println("A�adiendo comunidad a la base de datos . . . . .");
	//		out.println(setOnDataBase(nombre, comunidad, poblacion, cod_postal, direccion, "", gestoria, presidente, telefono));
			out.println(setOnDataBase(id_comunidad, comunidad_autonoma,provincia,poblacion, cod_postal, direccion, gestoria, presidente, telefono));
			
		} catch (IOException e) {
			
			out = resp.getWriter(  );
			out.println("No se ha podido a�adir, consulte con el administrador.");

		}finally {
						
		}
	}
	

	public String setOnDataBase(String id_comunidad, String comunidad_autonoma, String provincia, String poblacion, String cod_postal, String direccion, String gestoria, String presidente, String telefono){
		
		String resultado = "";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		String query = "INSERT INTO procomvecbbdd.comunidades "
			+ " (id_comunidad,comunidad_autonoma,provincia,poblacion,cod_postal,direccion,gestoria,presidente,telefono)"
			+ " VALUES ('"+id_comunidad+"','"+comunidad_autonoma+"','"+provincia+"','"+poblacion+"','"+cod_postal+"','"+direccion+"','"+gestoria+"','"+presidente+"','"+telefono+"');";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Comunidad a�adida";
	
		} catch (SQLException ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede a�adir la comunidad";
	
		}
		
		return resultado;

	}
	
}
