<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ page import = "com.admin.consults.vecinos.ListaVecinos"%> 
 <%@ page import = "com.admin.objects.Vecinos"%> 
 <%@ page import = "java.util.ArrayList"%> 
 <%@ page import = "com.admin.objects.Comunidades"%> 
 <%@ page import = "com.admin.consults.comunidades.ListaComunidades"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>addVecinos</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<form action="/addVecino" id="form" method="post">
	<table>	
		<tr><td  class='texto' style='width: 10%'>Nombre</td><td style='width:10px'></td><td><input class='marco_opciones' name='nombre'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Apellido 1</td><td style='width:10px'></td><td><input class='marco_opciones' name='apellido1'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Apellido 2</td><td style='width:10px'></td><td><input class='marco_opciones' name='apellido2'></td></tr>
		<tr><td  class='texto' style='width: 10%'>D.N.I</td><td style='width:10px'></td><td><input class='marco_opciones' name='dni'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Usuario</td><td style='width:10px'></td><td><input class='marco_opciones' name='user'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Contraseña</td><td style='width:10px'></td><td><input class='marco_opciones' name='password'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Plobación</td><td style='width:10px'></td><td><input class='marco_opciones' name='poblacion'></td></tr>
		<tr><td  class='texto' style='width: 10%'>C.P</td><td style='width:10px'></td><td><input class='marco_opciones' name='cod_postal'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Calle</td><td style='width:10px'></td><td><input class='marco_opciones' name='calle'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Número</td><td style='width:10px'></td><td><input class='marco_opciones' name='numero_calle'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Bloque</td><td style='width:10px'></td><td><input class='marco_opciones' name='bloque'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Piso</td><td style='width:10px'></td><td><input class='marco_opciones' name='piso'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Puerta</td><td style='width:10px'></td><td><input class='marco_opciones' name='puerta'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Comunidad</td><td style='width:10px'></td><td><select class='marco_opciones' style='width: 150px' name='comunidad'><%out.print(getComunidades(request.getParameter("user"))); %></select></td></tr>
		<tr><td  class='texto' style='width: 10%'>Teléfono</td><td style='width:10px'></td><td><input class='marco_opciones' name='telefono'></td></tr>
		<tr><td  class='texto' style='width: 10%'>Email</td><td style='width:10px'></td><td><input class='marco_opciones' name='email'></td></tr>
		
		
		<tr>
			<td style='width: 60px'><button class='marco_button'  style='width: 60px' onclick='submit'>Crear</button></td>
		</tr>
	</table>
</form>
</body>
<%!
	public String getComunidades(String user){
	
		String row = "";	
		ListaComunidades lc = new ListaComunidades();
		ArrayList<String> lista = new ArrayList<String>();
		
		try{
			lista = lc.getNombreComunidades(user);
		}catch(Exception e){
			
		}finally{
			
		}
		
		for (int i = 0; i < lista.size();i++){
			
			row = row +  "<option class='texto' value = "+lista.get(i)+">"+lista.get(i)+"</option>\n";
					
		}		
		
		return row;

}
		
%>	
</html>