package com.admin.consults.vecinos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.admin.objects.Vecinos;
import com.google.appengine.api.rdbms.AppEngineDriver;

public class GetVecino {

	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
	private Vecinos vecino;
		
	public GetVecino(){
		super();	
	}
	
	public Vecinos getVecino(String dni) throws  IOException, ClassNotFoundException{
				
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM procomvecbbdd.vecinos WHERE dni='"+dni+"';" ;

		try {	
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);	
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first();			
			vecino = new Vecinos (rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),rs.getString(16));
			
		}catch(SQLException e){
			
			
			
		}
		
		return vecino;
	}
	
	public Vecinos getVecinoById(String id) throws  IOException, ClassNotFoundException{
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM procomvecbbdd.vecinos WHERE id='"+id+"';" ;

		try {	
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);	
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first();			
			vecino = new Vecinos (rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),rs.getString(16),rs.getString(17),rs.getString(18));
			
		}catch(SQLException e){
			
			
			
		}
		
		return vecino;
	}
	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }	
		
	}

}
