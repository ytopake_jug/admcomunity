<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.admin.consults.comunidades.ListaComunidades"%> 
<%@ page import = "com.admin.objects.Comunidades"%> 
<%@ page import = "java.util.ArrayList"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ListaComunidades</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<table>
	<tr>
		<table>
			<% out.print(getListaComunidades(request.getParameter("user"))); %>
		</table>
	</tr>
</table>
</body>



<%!

	public String getListaComunidades (String user){
	
	//	String row = "<tr><td></td><td class='auto-style12'>Id Comunidad</td><td class='auto-style12'>Comunidad Autónoma</td><td class='auto-style12'>Provincia</td><td  class='auto-style12'>Poblacion</td><td  class='auto-style12'>C.P</td>";
	//	row = row + "<td  class='auto-style12'>Dirección</td><td  class='auto-style12'>Alta</td><td  class='auto-style12'>Gestoria</td><td  class='auto-style12'>Presidente</td>";
	//	row = row + "<td  class='auto-style12'>Telefono</td>";
	
		String row = "<tr><td class='auto-style12'>Id Comunidad</td>";
		row = row + "<td  class='auto-style12'>Dirección</td>";
		row = row + "<td  class='auto-style12'>Telefono</td>";
		
		ListaComunidades lc = new ListaComunidades();
		ArrayList<Comunidades> comunidades = new ArrayList<>();
		
		try{
			comunidades = lc.getListaComunidades(user);
		}catch (Exception io){
			
		}finally{		
			
		}
		
		for (int i = 0; i < comunidades.size(); i++){
			
			row = row + "<tr> " + "\n";
	//		row = row + "<td class='auto-style15' style='width: 10%'></td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10% ;cursor: pointer; border-radius: 5px; background-color: #3a9e05;' onclick='goListaComunidadDetalle(\""+comunidades.get(i).getId_comunidad()+"\")'>"+comunidades.get(i).getId_comunidad()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getComunidad_autonoma()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getProvincia()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getPoblacion()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getCod_postal()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getDireccion()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getFecha_alta()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getGestoria()+"</td>" + "\n";
	//		row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getPresidente()+"</td>" + "\n";
			row = row + "<td class='auto-style13' style='width: 10%'>"+comunidades.get(i).getTelefono()+"</td>" + "\n";
	//		row = row + "<td  class='auto-style11' style='width: 3%' onclick='editar(\""+comunidades.get(i).getId_comunidad()+"\")'>editar</td>" + "\n";
			row = row + "</tr>";
		}
		
		lc.closeConnection();
		return row;
	
	}
%>
<script>

	function editar (comunidad) {
		
		location.href = "/forms/subButtons/comunidades/edit.jsp?comunidad="+comunidad;	
		
	}
	
	
	function goListaComunidadDetalle (comunidad) {
		
		location.href = "/forms/ListaComunidadesDetalle.jsp?comunidad="+comunidad;	
		
	}

</script>
</html>