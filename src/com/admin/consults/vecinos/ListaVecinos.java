package com.admin.consults.vecinos;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.admin.objects.Vecinos;
import com.google.appengine.api.rdbms.AppEngineDriver;


public class ListaVecinos {

	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
	private ArrayList<Vecinos> listaVecinos;
		
	public ListaVecinos(){
		super();	
	}
	
	public ArrayList<Vecinos> getListaVecinos(String comunidad) throws  IOException, ClassNotFoundException{
		
		listaVecinos = new ArrayList<>();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM procomvecbbdd.vecinos WHERE comunidad ='"+comunidad+"';" ;

		try {	

			ResultSet rs = (ResultSet) stmt.executeQuery(query);	
			while(rs.next()){			
			listaVecinos.add(new Vecinos (rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),rs.getString(16),rs.getString(17),rs.getString(18)));
			}
		}catch(SQLException e){
			
			
			
		}
		return listaVecinos;
	}
	
	public ArrayList<Vecinos> getListaVecinos() throws  IOException, ClassNotFoundException{
		
		listaVecinos = new ArrayList<>();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM procomvecbbdd.vecinos ;" ;

		try {	

			ResultSet rs = (ResultSet) stmt.executeQuery(query);	
			while(rs.next()){			
			listaVecinos.add(new Vecinos (rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),rs.getString(7),rs.getString(8),rs.getString(9),rs.getString(10),rs.getString(11),rs.getString(12),rs.getString(13),rs.getString(14),rs.getString(15),rs.getString(16),rs.getString(17),rs.getString(18)));
			}
		}catch(SQLException e){
			
			
			
		}
		return listaVecinos;
	}
	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }
		
		
	}

}
