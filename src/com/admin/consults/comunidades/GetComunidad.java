package com.admin.consults.comunidades;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.admin.objects.Comunidades;
import com.google.appengine.api.rdbms.AppEngineDriver;

public class GetComunidad {

	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
	private Comunidades comunidad;
		
	public GetComunidad(){
		super();	
	}
	
	public Comunidades getComunidad(String id_comunidad) throws  IOException, ClassNotFoundException{
				
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT * FROM procomvecbbdd.comunidades WHERE id_comunidad='"+id_comunidad+"';" ;

		try {	
			stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);	
			rs = (ResultSet) stmt.executeQuery(query);	
			rs.first();			
			comunidad = new Comunidades (rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6),rs.getString(7), rs.getString(9), rs.getString(9),rs.getString(10),rs.getString(11));
			
		}catch(SQLException e){
			
			
			
		}
		
		return comunidad;
	}
	
	public ArrayList<String> getComunidades (String user) throws  IOException, ClassNotFoundException{
		
		ArrayList<String> comunidades = new ArrayList<String>();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String query = "SELECT comunidad FROM procomvecbbdd.adminusers WHERE nombre='"+user+"';" ;

		try {	

			ResultSet rs = (ResultSet) stmt.executeQuery(query);	
			while(rs.next()){			
				comunidades.add(rs.getString(1));
			}
		}catch(SQLException e){	}
		
		return comunidades;
	}
	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }	
		
	}

}
