package com.admin.objects;

public class Votaciones {
	
	private String id_vot;
	private String pregunta;
	private String opcion1;
	private String opcion2;
	private String opcion3;
	private String opcion4;
	private String opcion5;
	private String fecha_creacion;
	private String fecha_inicio;
	private String fecha_final;
	private String creador;
	private String vigencia;
	private String comunidad;
	
	public Votaciones () {
		
		super();
		
	}
	
	public Votaciones (String id_vot, String pregunta, String opcion1, String opcion2, String opcion3, String opcion4, String opcion5, String fecha_creacion, String fecha_inicio, String fecha_final, String creador, String vigencia, String comunidad) {
				
		this.id_vot = id_vot;
		this.pregunta = pregunta;
		this.opcion1 = opcion1;
		this.opcion2 = opcion2;
		this.opcion3 = opcion3;
		this.opcion4 = opcion4;
		this.opcion5 = opcion5;
		this.fecha_creacion = fecha_creacion;
		this.fecha_inicio = fecha_inicio;
		this.fecha_final = fecha_final;
		this.creador = creador;
		this.vigencia = vigencia;
		this.comunidad = comunidad;
		
	}

	public String getId_vot() {
		return id_vot;
	}

	public void setId_vot(String id_vot) {
		this.id_vot = id_vot;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getOpcion1() {
		return opcion1;
	}

	public void setOpcion1(String opcion1) {
		this.opcion1 = opcion1;
	}

	public String getOpcion2() {
		return opcion2;
	}

	public void setOpcion2(String opcion2) {
		this.opcion2 = opcion2;
	}

	public String getOpcion3() {
		return opcion3;
	}

	public void setOpcion3(String opcion3) {
		this.opcion3 = opcion3;
	}

	public String getOpcion4() {
		return opcion4;
	}

	public void setOpcion4(String opcion4) {
		this.opcion4 = opcion4;
	}

	public String getOpcion5() {
		return opcion5;
	}

	public void setOpcion5(String opcion5) {
		this.opcion5 = opcion5;
	}

	public String getFecha_creacion() {
		return fecha_creacion;
	}

	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}

	public String getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public String getFecha_final() {
		return fecha_final;
	}

	public void setFecha_final(String fecha_final) {
		this.fecha_final = fecha_final;
	}

	public String getCreador() {
		return creador;
	}

	public void setCreador(String creador) {
		this.creador = creador;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getComunidad() {
		return comunidad;
	}

	public void setComunidad(String comunidad) {
		this.comunidad = comunidad;
	}
	
	

}
