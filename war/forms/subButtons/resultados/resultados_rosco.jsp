<html>
<head>
  <title>Resultados Rosco</title>
  <link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['S�',     11],
          ['NO',      2],
          ['Abstenci�n',  2]
        ]);

        var options = {
          title: 'Resultado: Cambiar parab�lica'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
	</script>
<body>
<table>	
	<tr>
		<td>
			<div id="piechart" style="width: 900px; height: 500px;"></div>
		</td>
	</tr>
</table>
</body>
</html>