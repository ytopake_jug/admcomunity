package com.admin.consults.votaciones;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.rdbms.AppEngineDriver;

@SuppressWarnings("serial")
public class DelVotacion extends HttpServlet {
	
	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String id_votacion = req.getParameter("id_votacion");
				
		PrintWriter out;
		
		try {
			out = resp.getWriter(  );
			out.println("Eliminando votacion de la base de datos . . . . . " + id_votacion  );
			out.println(delOfDataBase(id_votacion));
			
		} catch (IOException e) {
			
			out = resp.getWriter(  );
			out.println("No se ha podido eliminar, consulte con el administrador.");

		}finally {
						
		}
	}
	

	public String delOfDataBase(String id_votacion){
		
		String resultado = "";
				
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		String query = "DELETE FROM procomvecbbdd.votaciones WHERE id_vot = '"+id_votacion+"'";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Votacion eliminada";
	
		} catch (Exception ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede eliminar la votacion";
	
		}
		
		return resultado;
		//return id;

	}
		
}
