package com.admin.consults.comunidades;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.admin.objects.Comunidades;
import com.google.appengine.api.rdbms.AppEngineDriver;


public class ListaComunidades {

	private Connection con = null;
	private ResultSet rs = null;
	private Statement stmt = null;
	private ArrayList<Comunidades> listaComunidades;
		
	public ListaComunidades(){
		super();	
	}
	
	
	public ArrayList<String> getNombreComunidades(String user) throws  IOException, ClassNotFoundException{
		
		ArrayList<String> nombresComunidades = new ArrayList<>();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		String query = "SELECT comunidad FROM procomvecbbdd.adminusers WHERE user ='"+user+"';" ;

		try {	

			ResultSet rs = (ResultSet) stmt.executeQuery(query);	
			while(rs.next()){			
				nombresComunidades.add(rs.getString(1));
			}
		}catch(SQLException e){	}
		
		return nombresComunidades;
	}
	
	public ArrayList<Comunidades> getListaComunidades(String user) throws  IOException, ClassNotFoundException{
		
		ArrayList<String> nombresComunidades = new ArrayList<>();
		
		nombresComunidades = getNombreComunidades(user);
		
		listaComunidades = new ArrayList<>();
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		for (int index = 0; index < nombresComunidades.size(); index++) {
			
			String query = "SELECT * FROM procomvecbbdd.comunidades WHERE id_comunidad ='"+nombresComunidades.get(index)+"';" ;

			try {	
				
				stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);	
				rs = (ResultSet) stmt.executeQuery(query);	
				rs.first();	
				listaComunidades.add(new Comunidades(rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getString(11)));
				
			}catch(SQLException e) {}
		
		
		}	
		
		return listaComunidades;
	}
	
	
	public void closeConnection(){
		
		try { rs.close(); } catch (Exception e) { /* ignored */ }
	    try { stmt.close(); } catch (Exception e) { /* ignored */ }
	    try { con.close(); } catch (Exception e) { /* ignored */ }
		
		
	}

}
