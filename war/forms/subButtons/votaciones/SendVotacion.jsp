<%@page import="com.admin.consults.votaciones.UpdateVotacion"%>
<%@page import="java.io.IOException"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "com.admin.consults.comunidades.GetComunidad"%> 
<%@ page import = "com.admin.consults.votaciones.GetVotaciones"%> 
<%@ page import = "com.admin.objects.Vecinos"%> 
<%@ page import = "com.admin.utilities.SendMailObject"%>
<%@ page import = "java.util.ArrayList"%>
<%@ page import = "java.util.Random"%> 
<%@ page import = "com.admin.consults.vecinos.ListaVecinos"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Envio de votaciones</title>
</head>
<body>
	<%		
		
		ListaVecinos lv = new ListaVecinos();
		ArrayList<Vecinos> vecinos = new ArrayList<>();
		
		try{
			vecinos = lv.getListaVecinos(request.getParameter("comunidad"));
		}catch (Exception io){
			
		}finally{		
			out.print("Enviando a ... "+ "</br>");
		}
		
		String codigo = generateCodeVotacion();
		
		for (int i = 0; i < vecinos.size(); i++){
			
			out.print(vecinos.get(i).getEmail() + "</br>");
			
			SendMailObject smo = new SendMailObject();
			out.print(smo.send(vecinos.get(i).getId(), vecinos.get(i).getNombre() + " " + vecinos.get(i).getApellido1() + " " + vecinos.get(i).getApellido2(), vecinos.get(i).getEmail(), codigo+vecinos.get(i).getId(), request.getParameter("id_vot")));
			
			UpdateVotacion uv = new UpdateVotacion();
			out.print(uv.updateCodeOfDataBase(codigo,request.getParameter("id_vot")));
			
		
		}
		
		lv.closeConnection();
		
		UpdateVotacion uv = new UpdateVotacion();
		out.print(uv.updateOfDataBase(request.getParameter("id_vot")));

%>
<%!

	public String generateCodeVotacion(){
	
		String code = "";

		for (int i = 0; i <20; i++){
			
			Random rand = new Random(); 
			int value = rand.nextInt(9); 
			code = code + Integer.toString(value);
			
		}
		
		return code;
	
	}

%>
</body>
</html>