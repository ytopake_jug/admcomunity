<%@page import="sun.reflect.ReflectionFactory.GetReflectionFactoryAction"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="es" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>admin-page</title>
<link href="/css/adminpage.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<table style="width: 100%; height: 800px;">
	<tr>
		<td style="height: 110px">
		<table style="width: 100%">
			<tr>
				<td style="width: 50%; height: 110px" class="auto-style7">
				<img alt="" height="110" src="/images/logoIzq.PNG" width="297" /></td>
				<td style="width: 50%; height: 110px" class="auto-style8">
				<img alt="" height="110" src="/images/logoDer.PNG" width="472" /></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td valign="top">
		<table style="width: 100%; height: 690px;">
			<tr>
				<td class="auto-style1" style="width: 15%" valign="top">
				<table class="auto-style2" style="width: 100%">
					<tr>
						<td class="auto-style5" style="height: 64px">
							<table>
								<tr>
									<td><b><font color='gray'>Nombre:</font></b></td>
									<td id='user' style='color:white'><%out.print(request.getParameter("user"));%></td>
								</tr>
								<tr>
									<td><b><font color='gray'>Tipo:</font></b></td>
									<td id='tipo' style='color:white'><%out.print(request.getParameter("tipo"));%></td>
								</tr>
							</table>							
						</td>
						<td class="auto-style9">&nbsp;</td>
					</tr>
					<tr>
						<td id="adm"class="auto-style4">&nbsp;&nbsp; Administrar</td>
						<td class="auto-style6" style="width: 24px; height: 24px">
						<img alt="" height="32" src="/images/admin-with-cogwheels.png" width="32" /></td>
					</tr>
					<tr>
						<td id="com" class="auto-style4">&nbsp;&nbsp; Comunidades</td>
						<td class="auto-style6" style="width: 24px; height: 24px">
						<img alt="" height="30" src="/images/enterprise.png" width="30" /></td>
					</tr>
					<tr>
						<td id="vec" class="auto-style4">&nbsp;&nbsp; Vecinos</td>
						<td class="auto-style6" style="width: 24px; height: 24px">
						<img alt="" height="32" src="/images/user.png" width="32" /></td>
					</tr>
					<tr>
						<td id="vot" class="auto-style4">&nbsp;&nbsp; Votaciones</td>
						<td class="auto-style6" style="width: 24px; height: 24px">
						<img alt="" height="32" src="/images/election-envelopes-and-box.png" width="32" /></td>
					</tr>
			<!--  	<tr>
						<td id="res" class="auto-style4">&nbsp;&nbsp; Resultados</td>
						<td class="auto-style6" style="width: 24px; height: 24px">
						<img alt="" height="32" src="/images/result.png" width="32" /></td>
					</tr>	-->
				</table>
				</td>
				<td style="width: 85%" class="auto-style6" valign="top">
				<table style="width: 100%">
					<tr>
						<td class="auto-style10" id="titulo1"></td>
					</tr>
					<tr>
						<td><div id="panel1"></div></td>
					</tr>
					<tr>
						<td style='width: 100% ; '><div id="panel2"></div></td>
					</tr>
					<tr>
						<td style='width: 100% ; vertical-align: top;'><div id="panel3"></div></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<script src="/js/adminpage.js"></script>

<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
</body>
</html>
