package com.admin.consults.votaciones;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.google.appengine.api.rdbms.AppEngineDriver;

public class UpdateVotacion {
	
	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	
	public UpdateVotacion() {
		
		super();
		
	}
	

	public String updateOfDataBase(String id_votacion){
		
		String resultado = "";
				
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		String query = "UPDATE procomvecbbdd.votaciones SET vigencia='si' WHERE id_vot = '"+id_votacion+"';";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			resultado = "Votacion lanzada.";
	
		} catch (Exception ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede lanzar la votacion por problemas de conexi�n con la base de datos.";
	
		}
		
		try {
			con.close();stmt.close();
		} catch (SQLException e) {e.printStackTrace();}
		
		return resultado;

	}
	

	public String updateCodeOfDataBase(String code, String id_votacion){
		
		String resultado = "";
				
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		String query = "UPDATE procomvecbbdd.votaciones SET codigo='"+code+"' WHERE id_vot = '"+id_votacion+"';";
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			resultado = "C�digo actualizado.";
	
		} catch (Exception ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede actualizar el c�digo por problemas de conexi�n con la base de datos.";
	
		}
		
		try {
			con.close();stmt.close();
		} catch (SQLException e) {e.printStackTrace();}
		
		return resultado;

	}
		
}
