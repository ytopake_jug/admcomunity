
var titulo1 = document.getElementById("titulo1");
var icono1 = document.getElementById("icono1");
var panel1 = document.getElementById("panel1");
var panel2 = document.getElementById("panel2");
var panel3 = document.getElementById("panel3");
var adm = document.getElementById("adm");
var vec = document.getElementById("vec");
var com = document.getElementById("com");
var vot = document.getElementById("vot");
// var res = document.getElementById("res");

adm.onclick = function (){	
	
	titulo1.innerHTML = "&nbsp;&nbsp;Administraci&oacute;n de administradores";
	panel1.innerHTML = "<table><tr><td id='adm_alta' class='auto-style11'>Alta</td><td id='adm_buscar' class='auto-style11'>Buscar</td></tr></table><hr>";
	adm.style.background= "#F2F2F2";
	admSubButtons();
}

adm.onmouseover = function(){
	
	adm.style.background= "#F2F2F2";

}

adm.onmouseout = function(){
	
	adm.style.background= "#C0C0C0";
	
}

vec.onclick = function (){	
	
	titulo1.innerHTML = "&nbsp;&nbsp;Administraci&oacute;n de vecinos";	
	panel1.innerHTML = "<table><tr><td id='vec_alta' class='auto-style11'>Alta</td><td id='vec_buscar' class='auto-style11'>Buscar</td></tr></table><hr>";
	vec.style.background= "#F2F2F2";
	vecSubButtons();
}

vec.onmouseover = function(){
	
	vec.style.background= "#F2F2F2";
	
}

vec.onmouseout = function(){
	
	vec.style.background= "#C0C0C0";
	
}

com.onclick = function (){	
	
	titulo1.innerHTML = "&nbsp;&nbsp;Administraci&oacute;n de comunidades";	
	panel1.innerHTML = "<table><tr><td id='com_alta' class='auto-style11'>Alta</td><td id='com_buscar' class='auto-style11'>Buscar</td></tr></table><hr>";
	com.style.background= "#F2F2F2";
	comSubButtons();

}

com.onmouseover = function(){
	
	com.style.background= "#F2F2F2";
	
}

com.onmouseout = function(){
	
	com.style.background= "#C0C0C0";
	
}

vot.onclick = function (){	
	
	titulo1.innerHTML = "&nbsp;&nbsp;Edici&oacute;n de votaciones";	
	panel1.innerHTML = "<table><tr><td id='vot_pendientes' class='auto-style11'>Pendientes</td><td id='vot_curso' class='auto-style11'>En curso</td><td id='vot_final' class='auto-style11'>Finalizadas</td><td id='vot_crear' class='auto-style11'>Crear</td></tr></table><hr>";
	vot.style.background= "#F2F2F2";
	votSubButtons();
}

vot.onmouseover = function(){
	
	vot.style.background= "#F2F2F2";
	
}

vot.onmouseout = function(){
	
	vot.style.background= "#C0C0C0";
}



 function admSubButtons(){
	
	var adm_alta = document.getElementById("adm_alta");
	var adm_buscar = document.getElementById("adm_buscar");
	panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaAdministradores.jsp'></object>";
	panel3.innerHTML = "<table><tr><td id='recargar_adm' class='auto-style11' style='width: 10%; height: 25px'>Recargar lista de administradores</td></tr></table>";
	
	recargar_adm();

	adm_alta.onclick = function (){
		
		panel2.innerHTML = "Alta de administrador";
		adm_alta.style.background= "#CAE7BB";adm_alta.style.color= "#3a9e05";
		adm_buscar.style.background= "#3a9e05";adm_buscar.style.color= "white";
		
	}

	adm_buscar.onclick = function (){
		
		adm_alta.style.background= "#3a9e05";adm_alta.style.color= "white";
		adm_buscar.style.background= "#CAE7BB";adm_buscar.style.color= "#3a9e05";
		
	}
	
 }
 
 function vecSubButtons(){
		
	 	var user = document.getElementById("user").innerHTML;
		var vec_alta = document.getElementById("vec_alta");
		var vec_buscar = document.getElementById("vec_buscar");
		panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaVecinos.jsp'></object>";
		panel3.innerHTML = "<table><tr><td id='recargar_vec' class='auto-style11' style='width: 10%; height: 25px'>Recargar lista de vecinos</td></tr></table>";
		
		recargar_vec();

		vec_alta.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100%; height: 580px' type='text/html' data='forms/subButtons/vecinos/add.jsp?user="+user+"'></object>";
			vec_alta.style.background= "#CAE7BB";vec_alta.style.color= "#3a9e05";
			vec_buscar.style.background= "#3a9e05";vec_buscar.style.color= "white";
			
		}

		vec_buscar.onclick = function (){
			
			vec_alta.style.background= "#3a9e05";vec_alta.style.color= "white";
			vec_buscar.style.background= "#CAE7BB";vec_buscar.style.color= "#3a9e05";
			
		}
		
 }
 
 function comSubButtons(){
		
	 	var user = document.getElementById("user").innerHTML;
		var com_alta = document.getElementById("com_alta");
		var com_buscar = document.getElementById("com_buscar");
		panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaComunidades.jsp?user="+user+"'></object>";
		panel3.innerHTML = "<table><tr><td id='recargar_com' class='auto-style11' style='width: 10%; height: 25px'>Recargar lista de comunidades</td></tr></table>";

		recargar_com();
		
		com_alta.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100%' type='text/html' data='forms/subButtons/comunidades/add.jsp'></object>";
			com_alta.style.background= "#CAE7BB";com_alta.style.color= "#3a9e05";
			com_buscar.style.background= "#3a9e05";com_buscar.style.color= "white";
			
		}

		com_buscar.onclick = function (){
			
			com_alta.style.background= "#3a9e05";com_alta.style.color= "white";
			com_buscar.style.background= "#CAE7BB";com_buscar.style.color= "#3a9e05";
			
		}
		
 }
 
 function votSubButtons(){
		
	 	var user = document.getElementById("user").innerHTML;
		var vot_crear = document.getElementById("vot_crear");
		var vot_curso = document.getElementById("vot_curso");
		var vot_final = document.getElementById("vot_final");
		var vot_pendientes = document.getElementById("vot_pendientes");
		panel2.innerHTML = "";
		panel3.innerHTML = "";
		
		vot_crear.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/subButtons/votaciones/CrearVotaciones.jsp?user="+user+"'></object>";
			vot_crear.style.background= "#CAE7BB";vot_crear.style.color = "#3a9e05";
			vot_curso.style.background= "#3a9e05";vot_curso.style.color = "white";
			vot_final.style.background= "#3a9e05";vot_final.style.color = "white";
			vot_pendientes.style.background= "#3a9e05";vot_pendientes.style.color = "white";
			
		}

		vot_curso.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaVotaciones.jsp?vigencia=si&user="+user+"'></object>";
			vot_crear.style.background= "#3a9e05";vot_crear.style.color = "white";
			vot_curso.style.background= "#CAE7BB";vot_curso.style.color = "#3a9e05";
			vot_final.style.background= "#3a9e05";vot_final.style.color = "white";
			vot_pendientes.style.background= "#3a9e05";vot_pendientes.style.color = "white";

			
			
		}
		
		vot_final.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaVotaciones.jsp?vigencia=no&user="+user+"'></object>";
			vot_crear.style.background= "#3a9e05";vot_crear.style.color = "white";
			vot_curso.style.background= "#3a9e05";vot_curso.style.color = "white";
			vot_final.style.background= "#CAE7BB";vot_final.style.color = "#3a9e05";
			vot_pendientes.style.background= "#3a9e05";vot_pendientes.style.color = "white";

			
		}
		
		vot_pendientes.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaVotaciones.jsp?vigencia=st&user="+user+"'></object>";
			vot_pendientes.style.background= "#CAE7BB";vot_pendientes.style.color = "#3a9e05";
			vot_crear.style.background= "#3a9e05";vot_crear.style.color = "white";
			vot_curso.style.background= "#3a9e05";vot_curso.style.color = "white";
			vot_final.style.background= "#3a9e05";vot_final.style.color = "white";
			
		}

				
}	

 function resSubButtons(){

	 	var user = document.getElementById("user").innerHTML;
		var res_ult = document.getElementById("res_ult");
		var res_his = document.getElementById("res_his");
		panel2.innerHTML = "";
		panel3.innerHTML = "";
		
		res_ult.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaResultados.jsp?clase=ultimas&user="+user+"'></object>";
			res_ult.style.background= "#CAE7BB";res_ult.style.color= "#3a9e05";
			res_his.style.background= "#3a9e05";res_his.style.color= "white";
			
		}

		res_his.onclick = function (){
			
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaResultados.jsp?clase=todas&user="+user+"'></object>";
			res_ult.style.background= "#3a9e05";res_ult.style.color= "white";
			res_his.style.background= "#CAE7BB";res_his.style.color= "#3a9e05";
			
		}
		
}
 
 
 function recargar_adm () {
		
		var recargar_adm = document.getElementById("recargar_adm");
		
		recargar_adm.onclick = function(){
			
			panel1.innerHTML = "<table><tr><td id='adm_alta' class='auto-style11'>Alta</td><td id='adm_buscar' class='auto-style11'>Buscar</td></tr></table><hr>";
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaAdministradores.jsp'></object>";
			panel3.innerHTML = "";
			admSubButtons();
			
		}		
		
}

	
 function recargar_vec () {
		
		var recargar_vec = document.getElementById("recargar_vec");
		
		recargar_vec.onclick = function(){
			
			panel1.innerHTML = "<table><tr><td id='vec_alta' class='auto-style11'>Alta</td><td id='vec_buscar' class='auto-style11'>Buscar</td></tr></table><hr>";
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaVecinos.jsp'></object>";
			panel3.innerHTML = "";
			vecSubButtons();
			
		}		
		
}
 
 function recargar_com () {
		
		var recargar_com = document.getElementById("recargar_com");
		
		recargar_com.onclick = function(){
			
			panel1.innerHTML = "<table><tr><td id='com_alta' class='auto-style11'>Alta</td><td id='com_buscar' class='auto-style11'>Buscar</td></tr></table><hr>";
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaComunidades.jsp'></object>";
			panel3.innerHTML = "";
			comSubButtons();
			
		}		
		
}
 
 /**function recargar_res () {
		
		var recargar_com = document.getElementById("recargar_res");
		
		recargar_res.onclick = function(){
			
			panel1.innerHTML = "<table><tr><td id='res_ult' class='auto-style11'>&Ucute;ltimas votaciones</td><td id='res_his' class='auto-style11'>Hist&ocute;rico</td></tr></table><hr>";
			panel2.innerHTML = "<object style='width: 100% ; height: 580px' type='text/html' data='forms/ListaResultados.jsp'></object>";
			panel3.innerHTML = "";
			resSubButtons();
			
		} 		
		
}**/
		
	

