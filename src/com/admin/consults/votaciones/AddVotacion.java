package com.admin.consults.votaciones;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.rdbms.AppEngineDriver;

@SuppressWarnings("serial")
public class AddVotacion extends HttpServlet {
	
	Connection con = null;
	Statement stmt = null;
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String pregunta = req.getParameter("pregunta");
		String opcion1 = req.getParameter("opcion1");
		String opcion2 = req.getParameter("opcion2");
		String opcion3 = req.getParameter("opcion3");
		String opcion4 = req.getParameter("opcion4");
		String opcion5 = req.getParameter("opcion5");
		String fecha_inicio = req.getParameter("fecha_inicio");
		String fecha_final = req.getParameter("fecha_final");
		String creador = req.getParameter("user");
		String comunidad = req.getParameter("comunidad");
		
		PrintWriter out;
		
		try {
			out = resp.getWriter(  );
			out.println("<b>A�adiendo votaci�n a la base de datos . . . . .</b></br>");
			out.println("<b>pregunta: </b>" + pregunta+"</br>");
			out.println("<b>opcion1: </b>" + opcion1+"</br>");
			out.println("<b>opcion2: </b>" + opcion2+"</br>");
			out.println("<b>opcion3: </b>" + opcion3+"</br>");
			out.println("<b>opcion4: </b>" + opcion4+"</br>");
			out.println("<b>opcion5: </b>" + opcion5+"</br>");
			out.println("<b>fecha_inicio: </b>" + fecha_inicio+"</br>");
			out.println("<b>fecha_final: </b>" + fecha_final+"</br>");
			out.println("<b>creador: </b>" + creador+"</br>");
			out.println("<b>comunidad: </b>" + comunidad+"</br>");
			out.println(setOnDataBase(pregunta,opcion1,opcion2,opcion3,opcion4,opcion5,fecha_inicio,fecha_final,creador,comunidad ));
			
		} catch (IOException e) {
			
			out = resp.getWriter(  );
			out.println("No se ha podido a�adir, consulte con el administrador.");

		}finally {
						
		}
	}
	

	public String setOnDataBase(String pregunta, String opcion1, String opcion2, String opcion3, String opcion4, String opcion5, String fecha_inicio, String fecha_final, String creador, String comunidad){
		
		String resultado = "";
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			DriverManager.registerDriver(new AppEngineDriver());
			con = DriverManager.getConnection(System.getProperty("procomvec"));	
			stmt = con.createStatement();
			
		} catch (ClassNotFoundException e1) {			
			e1.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
		String query = "INSERT INTO procomvecbbdd.votaciones (pregunta,opcion1,opcion2,opcion3,opcion4,opcion5,fecha_inicio,fecha_final,creador,vigencia,comunidad)"
			+ " VALUES ('"+pregunta+"','"+opcion1+"','"+opcion2+"','"+opcion3+"','"+opcion4+"','"+opcion5+"','"+fecha_inicio+"','"+fecha_final+"','"+creador+"','st','"+comunidad+"');";
		
		
		try {
		
			stmt = con.createStatement();
			stmt.executeUpdate(query);
			stmt.close();
			con.close();
			resultado = "Votaci�n a�adida";
	
		} catch (SQLException ex) {
		
			ex.printStackTrace();		
			resultado = "No se puede a�adir la votaci�n";
	
		}
		
		return resultado;

	}
	
}
